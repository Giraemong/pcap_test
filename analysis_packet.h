#ifndef ANALYSIS_PACKET_H
#define ANALYSIS_PACKET_H
#include <pcap.h>

void print_mac(const u_char* packet);
void print_ip(const u_char* packet);
void print_port(const u_char* packet);
void print_data(const u_char* packet, int size);

#endif