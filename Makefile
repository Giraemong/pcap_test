CC = gcc
TARGET = pcap_test
CFLAGS = -lpcap
OBJS = pcap_main.o analysis_packet.o

all : $(TARGET)

$(TARGET): $(OBJS)
	$(CC) -o $(TARGET) $(OBJS) $(CFLAGS)

pcap_main.o: pcap_main.c analysis_packet.h
	$(CC) -c pcap_main.c $(CFLAGS)

analysis_packet.o: analysis_packet.c analysis_packet.h
	$(CC) -c analysis_packet.c $(CFLAGS)

clean:
	rm -f pcap_test
	rm -f *.o

