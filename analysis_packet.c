#include "analysis_packet.h"
#include <stdio.h>
#include <pcap.h>
#include <stdint.h>

void print_mac(const u_char* packet) {
    printf("S_mac : %02X: %02X: %02X: %02X: %02X: %02X\n", packet[6], packet[7], packet[8], packet[9], packet[10], packet[11]);
    printf("D_mac : %02X: %02X: %02X: %02X: %02X: %02X\n", packet[0] ,packet[1], packet[2], packet[3], packet[4], packet[5]);
};

void print_ip(const u_char* packet) {
    printf("S_IP : %d.%d.%d.%d\n", packet[26] ,packet[27], packet[28], packet[29]);
    printf("D_IP : %d.%d.%d.%d\n", packet[30] ,packet[31], packet[32], packet[33]);
};

void print_port(const u_char* packet) {
    uint8_t hex_s_port[] = {packet[34], packet[35]};
    uint8_t hex_d_port[] = {packet[36], packet[37]};


    

    printf("src_tcp_port : %02X %02X\n", packet[34], packet[35]);
    printf("dst_tcp_port : %02X %02X\n", packet[36], packet[37]);
};

void print_data(const u_char* packet, int size) {
    int base = 66;

    printf("TCP data : ");
    for(int i = base; i < base + size; i++) {
        printf("%02X ", packet[i]);

    }
    printf("\n");
};

void usage() {
    printf("syntax: pcap_test <interface>\n");
    printf("sample: pcap_test wlan0\n");
};