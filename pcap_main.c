/*
This source is printed following data list using pcap library.

- layer2 source mac address, destination mac address.
- layer3 source ip address, destination ip address.
- layer4 source tcp port number, destination tcp port number.
- layer4 tcp's data.
  */

#include <stdio.h>
#include <stdint.h>
#include <pcap.h>
#include "analysis_packet.h"

void usage();

int main(int argc, char* argv[]) {

    if(argc != 2) {
        usage();
        return -1;
    }

    pcap_t* handle;
    char* dev, errbuf[PCAP_ERRBUF_SIZE];
    struct bpf_program fp;
    bpf_u_int32 net;
    struct pcap_pkthdr header;
    
    dev = argv[1];    
    handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);

    if(handle == NULL) {
        fprintf(stderr, "Couldn't open device :%s %s\n", dev, errbuf);
        return -1;
    }

    while(1) {
        struct pcap_pkthdr* header;
        const u_char* packet;

        int res = pcap_next_ex(handle, &header, &packet);

        if(res == 0) continue;
        if(res == -1 || res == -2) break;

        printf("================================================\n");
        print_mac(packet);

        if (packet[12] == 0x08 && packet[13] == 0x00) {
            print_ip(packet);
        }

        printf("TCP : %X\n", packet[23]);
        if(packet[23] == 0x06) print_port(packet);
        
        if((int)packet[17] > 52) {
            int size;


            if((size = (int)packet[17] - 52) >= 10) {
                size = 10;
                print_data(packet, size);
            }
            else if( 0 < size && size < 10) {
                print_data(packet, size);
            }

        }
        printf("Jacked a packet with length of [%d]\n", header->len);
        printf("================================================\n");
    }
    
    pcap_close(handle);
    return 0;
}